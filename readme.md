
# Advent of code 2020 solutions

🌟 🎄 🎁 🎅 🌟

https://adventofcode.com/2020

In December 2020, we held a competition with my colleagues at Dewesoft d.o.o. in which we compete, who will be
able to collect the most golden stars, by solving the puzzles on website adventofcode.com. Me and a few other
colleagues managed to collect them all. Most of the puzzles were pretty straightforward, but some of them
were quite time-consuming. Anyway, here are my solutions to the puzzles.

Merry Christmas!

Jan M - https://janm.si
