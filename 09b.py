import numpy as np


class Solution:

    def __init__(self):
        pass

    def n_sum(self, arr, target):
        arr1 = np.zeros(len(arr))
        arr1[0] = arr[0]
        for i in range(1, len(arr)):
            arr1[i] = arr1[i-1]+arr[i]

        i = 0
        j = 1
        while i < len(arr) and j < len(arr):
            b = arr1[i-1] if i > 0 else 0
            sum = arr1[j]-b
            if sum > target:
                i += 1
                if i == j: j += 1
            elif sum < target:
                j += 1
            else:
                break

        return i, j

    def solve(self):
        f = open("res/09a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        target = 375054920
        nums = []
        for l in lines:
            nums.append(int(l))

        i, j = self.n_sum(nums, target)
        range = nums[i:j+1]
        print(min(range)+max(range))


s = Solution()
s.solve()
