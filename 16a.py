import re


class Solution:

    def __init__(self):
        pass

    def get_invalid(self, ticket, rules, invalid):
        for val in ticket:
            ok = 0
            for r in rules.values():
                if (val >= r[0] and val <= r[1]) or (val >= r[2] and val <= r[3]):
                    ok += 1
            if ok == 0:
                invalid.append(val)

    def solve(self):
        f = open("res/16a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        rules = {}
        lines = parts[0].split("\n")
        for l in lines:
            m = re.match("([a-z\s]+): (\d+)-(\d+) or (\d+)-(\d+)", l)
            name = m.group(1)
            rules[name] = [int(m.group(2)), int(m.group(3)), int(m.group(4)), int(m.group(5))]

        tickets = []
        lines = parts[2].split("\n")
        for i in range(1, len(lines)):
            vals = lines[i].split(",")
            arr = []
            for v in vals: arr.append(int(v))
            tickets.append(arr)

        invalid = []
        for ticket in tickets:
            self.get_invalid(ticket, rules, invalid)

        error_rate = sum(invalid)
        print(error_rate)



s = Solution()
s.solve()
