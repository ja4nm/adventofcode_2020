import re
import collections

class Solution:

    def __init__(self):
        pass

    def eval_simple(self, nums):
        op = "+"
        r = 0

        for i in range(0, len(nums)):
            if i % 2 == 0:
                if op == "+": r += int(nums[i])
                elif op == "*": r *= int(nums[i])
            else:
                op = nums[i]

        return r

    def solve(self):
        f = open("res/18a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        total = 0

        for line in lines:
            buf = []
            stack = collections.deque()
            stack.append(buf)

            for i in range(0, len(line)):
                c = line[i]
                if c == "*" or c == "+":
                    stack[-1].append(c)
                elif c == " ":
                    continue
                elif c == "(":
                    stack.append([])
                elif c == ")":
                    r = self.eval_simple(stack.pop())
                    stack[-1].append(r)
                else:
                    stack[-1].append(int(c))

            r = self.eval_simple(stack[-1])
            total += r

        print(total)

s = Solution()
s.solve()
