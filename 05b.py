
class Solution:

    def __init__(self):
        pass

    def get_id(self, line):
        p1 = line[0:7]
        p2 = line[7:]

        row = [0, 127]
        for i in range(0, 7):
            if p1[i] == "F":
                row[1] = row[0] + int((row[1]-row[0])/2)
            else:
                row[0] = row[0] + int((row[1]-row[0])/2)+1

        col = [0, 7]
        for i in range(0, 3):
            if p2[i] == "L":
                col[1] = col[0] + int((col[1]-col[0])/2)
            else:
                col[0] = col[0] + int((col[1] - col[0])/2)+1

        return row[0]*8 + col[0]

    def solve(self):
        f = open("res/05a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        ids = []
        for l in lines:
            id = self.get_id(l)
            ids.append(id)

        ids = sorted(ids)
        for i in range(1, len(ids)):
            slope = ids[i]-ids[i-1]
            if slope > 1:
                print(ids[i-1]+1)
                break






s = Solution()
s.solve()
