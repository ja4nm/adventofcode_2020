import re
import numpy as np
import math


class Tile:

    def __init__(self):
        self.id = 0
        self.T = None
        self.bl = None
        self.br = None
        self.bb = None
        self.bt = None
        self.top = []
        self.right = []
        self.bottom = []
        self.left = []

    def load(self, tile):
        self.T = tile
        self.bt = self._get_border(tile, "t")
        self.br = self._get_border(tile, "r")
        self.bb = self._get_border(tile, "b")
        self.bl = self._get_border(tile, "l")

    def _get_border(self, tile, border):
        arr = []
        if border == "t": arr = tile[0]
        elif border == "r": arr = tile[:,-1]
        elif border == "b": arr = tile[-1]
        else: arr = tile[:,0]
        s = ""
        for i in range(0, len(arr)): s += str(int(arr[i]))
        return s

    def calculate_adj(self, tiles):
        self.top = set()
        self.right = set()
        self.bottom = set()
        self.left = set()
        for id in tiles:
            if id == self.id: continue
            for tile in tiles[id]:
                if self.bt == tile.bb: self.top.add(tile)
                if self.br == tile.bl: self.right.add(tile)
                if self.bb == tile.bt: self.bottom.add(tile)
                if self.bl == tile.br: self.left.add(tile)


class Solution:

    def __init__(self):
        pass

    def get_group(self, tile):
        rotations = [0, 1, 2, 3]
        flips = ["", "h", "v", "hv"]
        group = []
        for r in rotations:
            for f in flips:
                t = np.copy(tile)
                if r > 0:
                    t = np.rot90(t, r)
                if f == "h":
                    t = np.flip(t, 1)
                elif f == "v":
                    t = np.flip(t, 0)
                elif f == "hv":
                    t = np.flip(t, 1)
                    t = np.flip(t, 0)
                group.append(t)
        return group

    def get_corner(self, tiles):
        for id in tiles:
            group = tiles[id]
            for tile in group:
                if len(tile.top) == 0 and len(tile.left) == 0:
                    return tile
        return None

    def next_top_edge(self, tile, used):
        for t in tile.right:
            if t.id not in used and len(t.top) == 0:
                return t

    def next_left_edge(self, tile, used):
        for t in tile.bottom:
            if t.id not in used and len(t.left) == 0:
                return t

    def next_center_tile(self, tile_top, tile_left, used):
        possible = set().union(tile_top.bottom).intersection(tile_left.right)
        for t in possible:
            if t.id not in used:
                return t

    def copy_part(self, src, i0, j0, i1, j1, dest, i2, j2):
        for i in range(i0, i1):
            for j in range(j0, j1):
                dest[i2 + (i-i0)][j2 + (j-j0)] = src[i][j]


    def crop_tiles(self, img):
        W = len(img)
        w = len(img[0][0].bt)
        w1 = w-2
        out = np.zeros((w1*W, w1*W))

        for i in range(0, W):
            for j in range(0, W):
                self.copy_part(img[i][j].T, 1, 1, w-1, w-1, out, i*w1, j*w1)

        return out

    def mask_monster(self, i, j, img, img_out, mask):
        is_monster = True
        for i1 in range(0, mask.shape[0]):
            for j1 in range(0, mask.shape[1]):
                if mask[i1][j1] == 1 and img[i+i1][j+j1] == 0:
                    is_monster = False
                    break

        if is_monster:
            for i1 in range(0, mask.shape[0]):
                for j1 in range(0, mask.shape[1]):
                    if mask[i1][j1] == 1: img_out[i+i1][j+j1] = 0

        return is_monster

    def mask_monsters(self, img, img_out, mask):
        monsters = 0
        for i in range(0, img.shape[0]-mask.shape[0]):
            for j in range(0, img.shape[1]-mask.shape[1]):
                if self.mask_monster(i, j, img, img_out, mask):
                    monsters += 1
        return monsters

    def count_ones(self, img):
        cnt = 0
        for i in range(0, img.shape[0]):
            for j in range(0, img.shape[1]):
                if img[i][j] == 1: cnt += 1
        return cnt

    def solve(self):
        f = open("res/20a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        tiles = dict()
        for part in parts:
            lines = part.split("\n")
            m = re.match("Tile ([0-9]+):", lines[0])
            id = int(m.group(1))
            h = len(lines) - 1
            w = len(lines[1])
            tile = np.zeros((h, w))
            for i in range(0, h):
                for j in range(0, w):
                    tile[i][j] = 0 if lines[i + 1][j] == "." else 1

            group = self.get_group(np.array(tile))
            tiles_tmp = []
            for t1 in group:
                t = Tile()
                t.id = id
                t.load(t1)
                tiles_tmp.append(t)
            tiles[id] = tiles_tmp

        for id in tiles:
            group = tiles[id]
            for tile in group:
                tile.calculate_adj(tiles)

        used = set()
        W = int(math.sqrt(len(tiles.keys())))
        img = []
        for i in range(0, W):
            img.append([])
            for j in range(0, W): img[i].append(None)

        corner = self.get_corner(tiles)
        img[0][0] = corner
        used.add(corner.id)

        # top edge
        for i in range(1, W):
            t = self.next_top_edge(img[0][i-1], used)
            img[0][i] = t
            used.add(t.id)

        # left edge
        for i in range(1, W):
            t = self.next_left_edge(img[i-1][0], used)
            img[i][0] = t
            used.add(t.id)

        # center of image
        for i in range(1, W):
            for j in range(1, W):
                t = self.next_center_tile(img[i-1][j], img[i][j-1], used)
                img[i][j] = t
                used.add(t.id)

        # crop tile borders
        img2 = self.crop_tiles(img)

        # create monster mask
        mask_str = [
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "
        ]
        mask = np.zeros((len(mask_str), len(mask_str[0])))
        for i in range(0, mask.shape[0]):
            for j in range(0, mask.shape[1]):
                mask[i][j] = 1 if mask_str[i][j] == "#" else 0

        # count 1 (#) which are not part of any monster
        cnt = 0
        group = self.get_group(img2)
        for img3 in group:
            img_out = np.copy(img3)
            monsters = self.mask_monsters(img3, img_out, mask)
            if monsters > 0:
                cnt = self.count_ones(img_out)

        print(cnt)


s = Solution()
s.solve()
