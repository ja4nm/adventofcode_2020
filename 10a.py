

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/10a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        adapters = [0]
        for line in lines:
            adapters.append(int(line))

        adapters = sorted(adapters)
        adapters.append(adapters[-1] + 3)
        diffs = {}
        for i in range(1, len(adapters)):
            d = adapters[i]-adapters[i-1]
            if d not in diffs: diffs[d] = 0
            diffs[d] += 1

        d1 = diffs[1] if 1 in diffs else 0
        d3 = diffs[3] if 3 in diffs else 0
        print(d1*d3)



s = Solution()
s.solve()
