
class Solution:

    def __init__(self):
        pass

    def move_front(self, pos, angle, arg):
        i = 0
        d = 1
        if angle == 0:
            i = 0
            d = 1
        elif angle == 90:
            i = 1
            d = 1
        elif angle == 180:
            i = 0
            d = -1
        elif angle == 270:
            i = 1
            d = -1
        pos[i] += arg * d

    def solve(self):
        f = open("res/12a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        pos = [0, 0]
        angle = 90

        for l in lines:
            cmd = l[0]
            arg = int(l[1:])

            if cmd == "N":
                pos[0] += arg
            elif cmd == "S":
                pos[0] -= arg
            elif cmd == "E":
                pos[1] += arg
            elif cmd == "W":
                pos[1] -= arg
            elif cmd == "L":
                angle = (angle-arg) % 360
            elif cmd == "R":
                angle = (angle+arg) % 360
            elif cmd == "F":
                self.move_front(pos, angle, arg)

        distance = abs(pos[0])+abs(pos[1])
        print(distance)


s = Solution()
s.solve()
