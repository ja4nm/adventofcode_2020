
class Solution:

    def __init__(self):
        pass

    def count_trees(self, lines):
        cnt = 0
        jmp = [3, 1]
        W = len(lines[0])
        H = len(lines)
        i = 0
        j = 0
        while i < H:
            if lines[i][j % W] == "#": cnt += 1
            i += jmp[1]
            j += jmp[0]
        return cnt

    def solve(self):
        f = open("res/03a/input.txt", "r")
        lines = []
        while True:
            l = f.readline()
            if not l: break
            lines.append(l.strip())
        f.close()
        cnt = self.count_trees(lines)
        print(cnt)


s = Solution()
s.solve()
