import re

class Solution:

    def __init__(self):
        self.mask = ""
        self.mem = dict()
        pass

    def to_addr(self, positions, x):
        addr = 0
        j = 0
        for p in positions:
            if (0b1 << j) & x:
                addr |= (0b1 << p)
            j += 1
        return addr

    def mem_write(self, mask, addr, val):
        template = addr
        positions = []

        for i in range(0, 36):
            j = 36-i-1
            if mask[j] == "X":
                positions.append(i)
                template &= ~(0b1 << i)
            elif mask[j] == "1":
                template |= (0b1 << i)

        mem_max = 2**len(positions)
        for i in range(0, mem_max):
            addr2 = self.to_addr(positions, i) | template
            self.mem[addr2] = val

    def solve(self):
        f = open("res/14a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        for line in lines:
            if line.startswith("mask"):
                m = re.match("mask = ([X01]+)", line)
                self.mask = m.group(1)
            else:
                m = re.match("mem\[(\d+)\] = (\d+)", line)
                num = int(m.group(2))
                addr = int(m.group(1))
                self.mem_write(self.mask, addr, num)

        total = 0
        for num in self.mem.values():
            total += num
        print(total)


s = Solution()
s.solve()
