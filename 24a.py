import math
import copy

class Solution:

    def __init__(self):
        pass

    def get_pos(self, directions):
        x = 0.0
        y = 0.0
        h = math.sqrt(1.0**2 - 0.5**2)
        for d in directions:
            if d == "w":
                x -= h*2
            elif d == "e":
                x += h*2
            elif d == "nw":
                x -= h
                y += h+1
            elif d == "ne":
                x += h
                y += h+1
            elif d == "sw":
                x -= h
                y -= h+1
            elif d == "se":
                x += h
                y -= h+1
        return x, y

    def to_hash(self, x, y):
        x = int(round(x*1000))
        y = int(round(y*1000))
        return hash(str(x)+";"+str(y))


    def parse_directions(self, line):
        directions = []
        i = 0
        while i < len(line):
            if line[i] == "s" or line[i] == "n":
                directions.append(line[i:i+2])
                i += 2
            else:
                directions.append(line[i])
                i += 1
        return directions

    def solve(self):
        f = open("res/24a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        tiles = dict()

        for line in lines:
            d = self.parse_directions(line)
            x, y = self.get_pos(d)
            hash = self.to_hash(x, y)
            if hash not in tiles: tiles[hash] = 0
            tiles[hash] += 1

        black = 0
        for tile in tiles.values():
            if tile % 2 == 1: black += 1
        print(black)


s = Solution()
s.solve()
