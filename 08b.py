

class Solution:

    def __init__(self):
        pass

    def swap_run(self, swp, commands):
        for i in range(0, len(commands)): commands[i]["executed"] = False

        name = commands[swp]["name"]
        if name == "jmp": commands[swp]["name"] = "nop"
        else: commands[swp]["name"] = "jmp"

        acc = 0
        i = 0
        while i < len(commands):
            cmd = commands[i]
            if cmd["executed"]:
                break
            elif cmd["name"] == "jmp":
                i += cmd["arg"] - 1
            elif cmd["name"] == "acc":
                acc += cmd["arg"]
            cmd["executed"] = True
            i += 1

        ok = i >= len(commands)
        commands[swp]["name"] = name
        return ok, acc

    def solve(self):
        f = open("res/08a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        commands = []
        for line in lines:
            parts = line.split(" ")
            cmd = {
                "name": parts[0],
                "arg": int(parts[1]),
                "executed": False
            }
            commands.append(cmd)

        final_acc = -1

        for i in range(0, len(commands)):
            cmd = commands[i]
            if cmd["name"] == "nop" or cmd["name"] == "jmp":
                ok, acc = self.swap_run(i, commands)
                if ok:
                    final_acc = acc
                    break

        print(final_acc)


s = Solution()
s.solve()
