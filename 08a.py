

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/08a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        commands = []
        for line in lines:
            parts = line.split(" ")
            cmd = {
                "name": parts[0],
                "arg": int(parts[1]),
                "executed": False
            }
            commands.append(cmd)

        acc = 0
        i = 0
        while i < len(commands):
            cmd = commands[i]
            #print(i, cmd)
            if cmd["executed"]:
                break
            elif cmd["name"] == "jmp":
                i += cmd["arg"]-1
            elif cmd["name"] == "acc":
                acc += cmd["arg"]
            cmd["executed"] = True
            i += 1

        print(acc)


s = Solution()
s.solve()
