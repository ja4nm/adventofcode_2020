
class Bag:
    def __init__(self):
        self.color = ""
        self.ngold = 0


class Solution:

    def __init__(self):
        pass

    def parse_bag_line(self, line):
        line = line.rstrip(".")
        parts1 = line.split(" bags contain ", 1)
        color = parts1[0]
        parts2 = parts1[1].split(", ")
        child_bags = []

        for part in parts2:
            word = part.split(" ")
            if word[0] == "no": continue
            c = int(word[0])
            child_bags.append([c, word[1]+" "+word[2]])

        return color, child_bags

    def create_bag(self, color, bags, dp):
        if color in dp: return dp[color]

        bag = Bag()
        bag.ngold = 0
        bag.color = color

        children = bags[color]
        for child in children:
            c = self.create_bag(child[1], bags, dp)
            bag.ngold += c.ngold
            if c.color == "shiny gold": bag.ngold += 1

        dp[bag.color] = bag
        return bag

    def solve(self):
        f = open("res/07a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        bags = dict()
        for line in lines:
            color, children = self.parse_bag_line(line)
            bags[color] = children

        ngold = 0
        dp = dict()
        for color in bags.keys():
            bag = self.create_bag(color, bags, dp)
            if bag.ngold > 0: ngold += 1

        print(ngold)


s = Solution()
s.solve()
