import math

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/13a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        depart_after = int(lines[0])
        buses = []
        can_depart = []

        parts = lines[1].split(",")
        for part in parts:
            if part == "x": continue
            buses.append(int(part))

        for bus in buses:
            ts = math.ceil(depart_after/bus)*bus
            can_depart.append(ts)

        min_i = 0
        for i in range(0, len(can_depart)):
            if can_depart[i] < can_depart[min_i]:
                min_i = i

        r = (can_depart[min_i]-depart_after)*buses[min_i]
        print(r)


s = Solution()
s.solve()
