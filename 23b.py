
class Node:
    def __init__(self):
        self.next = None
        self.val = None

class List:
    def __init__(self):
        self.head = None
        self.nodes = dict()
        pass

    def from_arr(self, arr):
        node = Node()
        node.val = arr[0]
        self.head = node
        self.nodes[node.val] = node
        for i in range(1, len(arr)):
            n = Node()
            n.val = arr[i]
            node.next = n
            self.nodes[n.val] = n
            node = n
        node.next = self.head

    def print(self):
        node = self.head
        s = str(node.val) + " "
        node = node.next
        while node != self.head:
            s += str(node.val) + " "
            node = node.next
        print(s)

class Solution:

    def __init__(self):
        pass

    def move(self, src_node, dest_node):
        first = src_node.next
        last = src_node.next.next.next

        src_node.next = last.next
        tmp = dest_node.next
        dest_node.next = first
        last.next = tmp

    def solve(self):
        input_num = "952438716"

        print("initialising array ...")
        nums = []
        for i in range(0, len(input_num)):
            nums.append(int(input_num[i]))
        max_num = max(nums)
        for i in range(max_num+1, 1000001):
            nums.append(i)

        list = List()
        list.from_arr(nums)
        src_node = list.nodes[nums[0]]

        max_node = list.head
        node = max_node.next
        while node != list.head:
            if node.val > max_node.val: max_node = node
            node = node.next

        for i in range(0, 10000000):
            if i % 10000 == 0:
                print("progress: %.2f" % (i / 10000000))

            pickup = {src_node.next.val, src_node.next.next.val, src_node.next.next.next.val}
            dest = src_node.val-1
            while True:
                if dest not in list.nodes: dest = max_node.val
                if dest not in pickup: break
                dest -= 1
            dest_node = list.nodes[dest]
            self.move(src_node, dest_node)
            src_node = src_node.next

        node_one = list.nodes[1]
        a = node_one.next.val
        b = node_one.next.next.val
        print(a*b)


s = Solution()
s.solve()
