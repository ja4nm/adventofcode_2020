

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/21a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        foods = []
        all_allergens = set()

        for line in lines:
            line = line.strip(")")
            parts = line.split(" (contains ")
            ingredients = parts[0].split(" ")
            allergens = parts[1].split(", ")
            for a in allergens:
                all_allergens.add(a)
            food = {
                "ingredients": ingredients,
                "allergens": allergens
            }
            foods.append(food)

        can_contain = {}
        for food in foods:
            for ing in food["ingredients"]:
                can_contain[ing] = all_allergens.copy()

        for food in foods:
            for a in food["allergens"]:
                # missing ingredients can't contain a present allergen
                for ing in can_contain:
                    if ing not in food["ingredients"]:
                        if a in can_contain[ing]: can_contain[ing].remove(a)

        contains = []

        while len(can_contain) > 0:
            a = None

            for ing in can_contain:
                c = len(can_contain[ing])
                if c == 0:
                    continue
                elif c == 1:
                    a = next(iter(can_contain[ing]))
                    contains.append([ing, a])
                    break

            # remove found allergen
            if a is not None:
                for ing in can_contain:
                    if a in can_contain[ing]:
                        can_contain[ing].remove(a)
            else:
                break

        contains.sort(key=lambda e: e[1])
        r = ""
        for i in range(0, len(contains)):
            if i > 0: r += ","
            r += contains[i][0]

        print(r)



s = Solution()
s.solve()
