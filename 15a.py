

class Solution:

    def __init__(self):
        pass

    def next_num(self, i, n, mem):
        if n not in mem: r = 0
        else: r = i-mem[n]
        mem[n] = i
        return r

    def solve(self):
        f = open("res/15a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split(",")

        nums = []
        for l in lines:
            nums.append(int(l))

        mem = dict()
        for i in range(0, len(nums)):
            mem[nums[i]] = i

        target = 2020
        nprev = nums[-1]
        for i in range(len(nums)-1, target-1):
            n = self.next_num(i, nprev, mem)
            nprev = n

        print(nprev)


s = Solution()
s.solve()
