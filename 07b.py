
class Bag:
    def __init__(self):
        self.color = ""
        self.nbags = 0

class Solution:

    def __init__(self):
        pass

    def parse_bag_line(self, line):
        line = line.rstrip(".")
        parts1 = line.split(" bags contain ", 1)
        color = parts1[0]
        parts2 = parts1[1].split(", ")
        child_bags = []

        for part in parts2:
            word = part.split(" ")
            if word[0] == "no": continue
            c = int(word[0])
            child_bags.append([c, word[1]+" "+word[2]])

        return color, child_bags

    def create_bag(self, color, bags, dp):
        bag = Bag()
        bag.nbags = 0
        bag.color = color

        children = bags[color]
        for child in children:
            c = self.create_bag(child[1], bags, dp)
            bag.nbags += child[0]*(c.nbags+1)

        return bag

    def solve(self):
        f = open("res/07a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        bags = dict()
        for line in lines:
            color, children = self.parse_bag_line(line)
            bags[color] = children

        dp = dict()
        bag = self.create_bag("shiny gold", bags, dp)
        print(bag.nbags)


s = Solution()
s.solve()
