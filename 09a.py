import collections


class Solution:

    def __init__(self):
        pass

    def two_sum(self, arr, n):
        arr = sorted(arr)
        i = 0
        j = len(arr)-1
        while i < j and j >= 0:
            s = arr[i]+arr[j]
            if s > n: j -= 1
            elif s < n: i += 1
            else: break
        return i < j

    def solve(self):
        f = open("res/09a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        nums = []
        for l in lines:
            nums.append(int(l))

        preamble_len = 25
        preamble = collections.deque(nums[0:preamble_len])
        N = None

        for i in range(preamble_len, len(nums)):
            n = nums[i]
            ok = self.two_sum(preamble, n)
            if not ok:
                N = n
                break
            preamble.popleft()
            preamble.append(n)

        print(N)






s = Solution()
s.solve()
