import re
import numpy as np


class Solution:

    def __init__(self):
        pass

    def is_valid(self, ticket, rules):
        for val in ticket:
            ok = 0
            for r in rules.values():
                if (val >= r[0] and val <= r[1]) or (val >= r[2] and val <= r[3]):
                    ok += 1
            if ok == 0:
                return False
        return True

    def possible_cols(self, r, tickets):
        arr = []
        for j in range(0, tickets.shape[1]):
            c = 0
            for i in range(0, tickets.shape[0]):
                v = tickets[i][j]
                if (v >= r[0] and v <= r[1]) or (v >= r[2] and v <= r[3]):
                    c += 1
            if c == tickets.shape[0]: arr.append(j)
        return arr

    def solve(self):
        f = open("res/16a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        rules = {}
        lines = parts[0].split("\n")
        for l in lines:
            m = re.match("([a-z\s]+): (\d+)-(\d+) or (\d+)-(\d+)", l)
            name = m.group(1)
            rules[name] = [int(m.group(2)), int(m.group(3)), int(m.group(4)), int(m.group(5))]

        nums = parts[1].split("\n")[1].split(",")
        my_ticket = []
        for n in nums: my_ticket.append(int(n))

        tickets = []
        lines = parts[2].split("\n")
        for i in range(1, len(lines)):
            vals = lines[i].split(",")
            arr = []
            for v in vals: arr.append(int(v))
            tickets.append(arr)

        valid_tickets = []
        tickets.append(my_ticket)
        for ticket in tickets:
            if self.is_valid(ticket, rules): valid_tickets.append(ticket)
        valid_tickets = np.array(valid_tickets)

        possible_cols = []
        for key in rules:
            r = rules[key]
            arr = self.possible_cols(r, valid_tickets)
            possible_cols.append([key, arr])
        possible_cols = sorted(possible_cols, key=lambda x: len(x[1]))

        cls_map = {}
        used = set()
        for entry in possible_cols:
            cls = entry[0]
            for i in range(0, len(entry[1])):
                if entry[1][i] not in used:
                    cls_map[cls] = entry[1][i]
                    used.add(entry[1][i])
                    break

        ret = 1
        for cls in cls_map:
            if cls.startswith("departure"):
                j = cls_map[cls]
                ret *= my_ticket[j]

        print(ret)















s = Solution()
s.solve()
