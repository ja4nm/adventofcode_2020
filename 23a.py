
class Node:
    def __init__(self):
        self.next = None
        self.val = None

class List:
    def __init__(self):
        self.head = None
        self.nodes = dict()
        pass

    def from_arr(self, arr):
        node = Node()
        node.val = arr[0]
        self.head = node
        self.nodes[node.val] = node
        for i in range(1, len(arr)):
            n = Node()
            n.val = arr[i]
            node.next = n
            self.nodes[n.val] = n
            node = n
        node.next = self.head

    def print(self):
        node = self.head
        s = str(node.val) + " "
        node = node.next
        while node != self.head:
            s += str(node.val) + " "
            node = node.next
        print(s)

class Solution:

    def __init__(self):
        pass

    def move(self, src_node, dest_node):
        first = src_node.next
        last = src_node.next.next.next

        src_node.next = last.next
        tmp = dest_node.next
        dest_node.next = first
        last.next = tmp

    def solve(self):
        input_num = "952438716"
        nums = []
        for i in range(0, len(input_num)):
            nums.append(int(input_num[i]))

        list = List()
        list.from_arr(nums)
        src_node = list.nodes[nums[0]]

        max_node = list.head
        node = max_node.next
        while node != list.head:
            if node.val > max_node.val: max_node = node
            node = node.next

        for i in range(0, 100):
            pickup = {src_node.next.val, src_node.next.next.val, src_node.next.next.next.val}
            dest = src_node.val-1
            while True:
                if dest not in list.nodes: dest = max_node.val
                if dest not in pickup: break
                dest -= 1
            dest_node = list.nodes[dest]
            self.move(src_node, dest_node)
            src_node = src_node.next

        r = ""
        node_one = list.nodes[1]
        node = node_one.next
        while node != node_one:
            r += str(node.val)
            node = node.next
        print(r)

s = Solution()
s.solve()
