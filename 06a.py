
class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/06a/input.txt", "r")
        content = f.read().rstrip()

        count = 0
        groups = content.split("\n\n")
        for group in groups:
            q = set()
            for c in group:
                if c == "\n": continue
                q.add(c)
            count += len(q)

        print(count)


s = Solution()
s.solve()
