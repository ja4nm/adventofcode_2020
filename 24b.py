import math
import copy
import matplotlib.pyplot as plt


class Solution:

    def __init__(self):
        pass

    def get_pos(self, directions):
        x = 0.0
        y = 0.0
        h = math.sqrt(1.0**2 - 0.5**2)
        for d in directions:
            if d == "w":
                x -= h*2
            elif d == "e":
                x += h*2
            elif d == "nw":
                x -= h
                y += h+1
            elif d == "ne":
                x += h
                y += h+1
            elif d == "sw":
                x -= h
                y -= h+1
            elif d == "se":
                x += h
                y -= h+1
        return x, y

    def count_adj_black(self, x, y, tiles):
        black = 0
        h = math.sqrt(1.0 ** 2 - 0.5 ** 2)
        adj = [
            [-h*2, 0],
            [h*2, 0],
            [-h, h+1],
            [h, h+1],
            [-h, -h-1],
            [h, -h-1]
        ]

        for p in adj:
            h = self.to_hash(x+p[0], y+p[1])
            if h in tiles:
                if tiles[h] % 2 == 1: black += 1

        return black

    def to_hash(self, x, y):
        x = int(round(x*1000))
        y = int(round(y*1000))
        return hash(str(x)+";"+str(y))


    def parse_directions(self, line):
        directions = []
        i = 0
        while i < len(line):
            if line[i] == "s" or line[i] == "n":
                directions.append(line[i:i+2])
                i += 2
            else:
                directions.append(line[i])
                i += 1
        return directions

    def ij_to_pos(self, i, j):
        h = math.sqrt(1.0 ** 2 - 0.5 ** 2)
        x = j*h*2
        y = i*(h+1)
        if abs(i) % 2 == 1: x += h
        return x, y

    def solve(self):
        f = open("res/24a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        tiles = []
        tiles_map = dict()
        for line in lines:
            d = self.parse_directions(line)
            x, y = self.get_pos(d)
            hash = self.to_hash(x, y)
            if hash not in tiles_map:
                tiles_map[hash] = 0
                tiles.append([x, y])
            tiles_map[hash] += 1

        max_chord = 0
        for t in tiles:
            if abs(t[0]) > max_chord: max_chord = abs(t[0])
            if abs(t[1]) > max_chord: max_chord = abs(t[1])
        max_offset = int(max_chord+1)

        for day in range(0, 100):
            black = 0
            tiles_map_tmp = copy.deepcopy(tiles_map)
            extend_offset = False

            for i in range(-max_offset, max_offset+1):
                for j in range(-max_offset, max_offset+1):
                    x, y = self.ij_to_pos(i, j)
                    h = self.to_hash(x, y)
                    is_black = h in tiles_map and tiles_map[h] % 2 == 1
                    adj_black = self.count_adj_black(x, y, tiles_map)
                    if is_black and (adj_black == 0 or adj_black > 2):
                        tiles_map_tmp[h] = 0
                    if not is_black and adj_black == 2:
                        tiles_map_tmp[h] = 1

                    if h in tiles_map_tmp and tiles_map_tmp[h] % 2 == 1:
                        black += 1
                        if i == -max_offset or i == max_offset or j == -max_offset or j == max_offset:
                            extend_offset = True

            if extend_offset: max_offset += 1
            tiles_map = tiles_map_tmp
            print("Day %d: %d" % (day+1, black))


s = Solution()
s.solve()
