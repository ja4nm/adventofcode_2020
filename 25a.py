

class Solution:

    def __init__(self):
        self.mod = 20201227
        pass

    def transform(self, subject, n):
        return pow(subject, n, self.mod)

    def solve(self):
        f = open("res/25a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        pub_card = int(lines[0])
        pub_door = int(lines[1])

        size_card = None
        for i in range(0, self.mod):
            if i % 100 == 0:
                print("progress: %.2f" % (i / self.mod))
            if pub_card == self.transform(7, i):
                size_card = i
                break

        key = self.transform(pub_door, size_card)
        print(key)


s = Solution()
s.solve()
