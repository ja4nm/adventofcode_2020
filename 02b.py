import re


class Solution:

    def __init__(self):
        pass

    def is_valid(self, a, b, chr, s):
        c = 0
        if a > 0 and a <= len(s) and s[a-1] == chr: c += 1
        if b > 0 and b <= len(s) and s[b-1] == chr: c += 1
        return c == 1

    def solve(self):
        f = open("res/02a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        valid = 0

        for line in lines:
            m = re.search("(\d+)-(\d+) (\w): (.*)", line)
            a = int(m.group(1))
            b = int(m.group(2))
            chr = m.group(3)
            password = m.group(4)
            if self.is_valid(a, b, chr, password):
                valid += 1

        print(valid)


s = Solution()
s.solve()
