import re

class Solution:

    def __init__(self):
        self.valid_colors = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
        pass

    def validate_height(self, value):
        matches = re.match(r"^([0-9]+)(cm|in)$", value)
        if not matches: return False
        num = int(matches.groups()[0])
        unit = matches.groups()[1]
        if unit == "cm":
            return num >= 150 and num <= 193
        elif unit == "in":
            return num >= 59 and num <= 76
        else:
            return False

    def is_passport_valid(self, passport):
        if "byr" not in passport: return False
        byr = int(passport["byr"])
        if byr < 1920 or byr > 2002: return False

        if "iyr" not in passport: return False
        iyr = int(passport["iyr"])
        if iyr < 2010 or iyr > 2020: return False

        if "eyr" not in passport: return False
        eyr = int(passport["eyr"])
        if eyr < 2020 or eyr > 2030: return False

        if "hgt" not in passport: return False
        hgt = passport["hgt"]
        if not self.validate_height(hgt): return False

        if "hcl" not in passport: return False
        hcl = passport["hcl"]
        if not re.match(r"^#[a-f0-9]{6}$", hcl): return False

        if "ecl" not in passport: return False
        ecl = passport["ecl"]
        if ecl not in self.valid_colors: return False

        if "pid" not in passport: return False
        pid = passport["pid"]
        if not re.match(r"^[0-9]{9}$", pid): return False

        return True

    def solve(self):
        f = open("res/04a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        valid = 0
        for part in parts:
            found = re.findall(r"([a-z]+):([^\s]+)", part, re.MULTILINE)
            p = {}
            for item in found:
                p[item[0]] = item[1]
            if self.is_passport_valid(p):
                valid += 1

        print(valid)


s = Solution()
s.solve()
