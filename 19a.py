import re


class Solution:

    def __init__(self):
        pass

    def build_regex(self, i, rules, dp):
        if i in dp: return dp[i]
        regex = ""

        if rules[i]["chr"] is not None:
            regex += rules[i]["chr"]
        else:
            sub = []
            for j in range(0, len(rules[i]["sub"])):
                r1 = ""
                for k in range(0, len(rules[i]["sub"][j])):
                    r1 += self.build_regex(rules[i]["sub"][j][k], rules, dp)
                sub.append(r1)
            regex += "("+("|".join(sub))+")"

        dp[i] = regex
        return regex

    def solve(self):
        f = open("res/19a/input.txt", "r")
        content = f.read().rstrip()
        sections = content.split("\n\n")

        # parse rules
        rules_text = sections[0].split("\n")
        rules = [None for x in range(len(rules_text))]
        for i in range(0, len(rules_text)):
            j = int(rules_text[i].split(":")[0])
            r1 = rules_text[i].split(":")[1].strip(" ")
            if r1.startswith("\""):
                it = {
                    "chr": r1.strip("\""),
                    "sub": None,
                }
                rules[j] = it
            else:
                r2 = r1.split("|")
                sub = []
                for r3 in r2:
                    arr = []
                    r4 = r3.strip(" ").split(" ")
                    for num in r4: arr.append(int(num))
                    sub.append(arr)

                it = {
                    "chr": None,
                    "sub": sub
                }
                rules[j] = it

        # parse messages
        messages = sections[1].strip("\n").split("\n")

        # count valid
        matches = 0
        dp = dict()
        regex = self.build_regex(0, rules, dp)

        for msg in messages:
            ok = re.match("^"+regex+"$", msg)
            if ok: matches += 1

        print(matches)


s = Solution()
s.solve()

