
'''
   1  3  5  7  9
1  2  4  6  8 10
3  4  9  8 10 12
5  6  8 10 12 14
7
9
'''

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/01a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        arr = []
        for line in lines:
            arr.append(int(line))

        arr.sort()
        target = 2020
        i = 0
        j = len(arr)-1
        while i < j:
            v = arr[i]+arr[j]
            if v > target: j -= 1
            elif v < target: i += 1
            else: break

        r = arr[i]*arr[j]
        print(r)


s = Solution()
s.solve()
