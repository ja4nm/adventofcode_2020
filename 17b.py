import numpy as np
import copy

class Solution:

    def __init__(self):
        self.N = 0
        self.cube = None
        self.cube_tmp = None
        pass

    def in_bounds(self, w, z, y, x):
        return w >= 0 and w < self.N and x >= 0 and x < self.N and y >= 0 and y < self.N and z >= 0 and z < self.N

    def ch_point(self, w, z, y, x):
        c = 0

        for dw in range(-1, 2):
            for dz in range(-1, 2):
                for dy in range(-1, 2):
                    for dx in range(-1, 2):
                        if dw == 0 and dz == 0 and dy == 0 and dx == 0: continue
                        if self.in_bounds(w+dw, z+dz, y+dy, x+dx) and self.cube[w+dw][z+dz][y+dy][x+dx] == 1:
                            c += 1

        if self.cube[w][z][y][x] == 1:
            if c == 2 or c == 3: pass
            else: self.cube_tmp[w][z][y][x] = 0
        else:
            if c == 3: self.cube_tmp[w][z][y][x] = 1
            else: pass

    def solve(self):
        f = open("res/17a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        max_cycles = 6
        self.M = len(lines)
        self.N = self.M+max_cycles*2
        C = int((self.M-1)/2)
        cube = np.zeros((self.N, self.N, self.N, self.N))
        for i in range(0, self.M):
            for j in range(0, self.M):
                if lines[i][j] == "#": cube[C+max_cycles][C+max_cycles][i+max_cycles][j+max_cycles] = 1
        self.cube = cube

        for cycle in range(0, max_cycles):
            self.cube_tmp = copy.deepcopy(self.cube)

            for w in range(0, self.N):
                print("cycle: %d w: %d" % (cycle, w))
                for z in range(0, self.N):
                    for y in range(0, self.N):
                        for x in range(0, self.N):
                            self.ch_point(w, z, y, x)

            self.cube = self.cube_tmp

        active = 0
        for w in range(0, self.N):
            for z in range(0, self.N):
                for y in range(0, self.N):
                    for x in range(0, self.N):
                        if self.cube[w][z][y][x] == 1: active += 1

        print("active cubes: %d" % active)

s = Solution()
s.solve()
