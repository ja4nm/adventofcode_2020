
class Solution:

    def __init__(self):
        pass

    def rotate_waypoint(self, waypoint, angle):
        # 10 1
        # -1 10
        # -10 -1
        # 1 -10
        tmp = waypoint[:]
        if angle == 90:
            waypoint[0] = -tmp[1]
            waypoint[1] = tmp[0]
        elif angle == 180:
            waypoint[0] = -tmp[0]
            waypoint[1] = -tmp[1]
        elif angle == 270:
            waypoint[0] = tmp[1]
            waypoint[1] = -tmp[0]

    def solve(self):
        f = open("res/12a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        ship_pos = [0, 0]
        waypoint = [1, 10]

        for l in lines:
            cmd = l[0]
            arg = int(l[1:])

            if cmd == "N":
                waypoint[0] += arg
            elif cmd == "S":
                waypoint[0] -= arg
            elif cmd == "E":
                waypoint[1] += arg
            elif cmd == "W":
                waypoint[1] -= arg
            elif cmd == "L":
                self.rotate_waypoint(waypoint, -arg % 360)
            elif cmd == "R":
                self.rotate_waypoint(waypoint, arg % 360)
            elif cmd == "F":
                ship_pos[0] += waypoint[0]*arg
                ship_pos[1] += waypoint[1]*arg

        distance = abs(ship_pos[0])+abs(ship_pos[1])
        print(distance)


s = Solution()
s.solve()
