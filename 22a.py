import collections

class Solution:

    def get_score(self, cards):
        cards = list(cards)
        cards.reverse()
        s = 0
        for i in range(0, len(cards)):
            s += (i+1)*cards[i]
        return s

    def solve(self):
        f = open("res/22a/input.txt", "r")
        content = f.read().rstrip()

        cards1 = []
        cards2 = []

        players = content.split("\n\n")
        lines = players[0].split("\n")
        for i in range(1, len(lines)):
            cards1.append(int(lines[i]))
        lines = players[1].split("\n")
        for i in range(1, len(lines)):
            cards2.append(int(lines[i]))

        cards1 = collections.deque(cards1)
        cards2 = collections.deque(cards2)

        while len(cards1) > 0 and len(cards2) > 0:
            c1 = cards1.popleft()
            c2 = cards2.popleft()
            if c1 > c2:
                cards1.append(c1)
                cards1.append(c2)
            else:
                cards2.append(c2)
                cards2.append(c1)

        if len(cards1) > 0: score = self.get_score(cards1)
        else: score = self.get_score(cards2)
        print(score)


s = Solution()
s.solve()
