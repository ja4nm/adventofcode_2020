import re
import numpy as np


class Solution:

    def __init__(self):
        pass

    def print_tile(self, tile):
        h = len(tile)
        w = len(tile[0])
        s = ""

        for i in range(0, h):
            for j in range(0, w):
                s += str(tile[i][j])
            s += "\n"

        print(s.rstrip("\n"))

    def get_borders(self, tile):
        borders = dict()
        # top
        borders["t"] = "".join(tile[0])
        borders["tm"] = borders["t"][::-1]
        # right
        borders["r"] = "".join(tile[:,-1])
        borders["rm"] = borders["r"][::-1]
        # bottom
        borders["b"] = "".join(tile[-1])
        borders["bm"] = borders["b"][::-1]
        # left
        borders["l"] = "".join(tile[:,0])
        borders["lm"] = borders["l"][::-1]

        return borders

    def solve(self):
        f = open("res/20a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        tiles = dict()

        for part in parts:
            lines = part.split("\n")
            m = re.match("Tile ([0-9]+):", lines[0])
            id = int(m.group(1))
            h = len(lines)-1
            w = len(lines[1])
            tile = []
            for i in range(0, h):
                tile.append([])
                for j in range(0, w):
                    tile[i].append(lines[i+1][j])
            tiles[id] = np.array(tile)

        borders_map = dict()
        for id in tiles:
            tile = tiles[id]
            borders = self.get_borders(tile).values()
            for b in borders:
                if b not in borders_map: borders_map[b] = []
                borders_map[b].append(id)

        neighbours_map = dict()
        for id in tiles:
            tile = tiles[id]
            borders = self.get_borders(tile).values()
            neighbours = set()
            for b in borders:
                for i in borders_map[b]: neighbours.add(i)
            neighbours.remove(id)
            neighbours_map[id] = neighbours

        corners = []
        for id in neighbours_map:
            if len(neighbours_map[id]) == 2: corners.append(id)

        m = 1
        for c in corners: m *= c
        print(m)


s = Solution()
s.solve()

