

class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/21a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        foods = []
        all_allergens = set()

        for line in lines:
            line = line.strip(")")
            parts = line.split(" (contains ")
            ingredients = parts[0].split(" ")
            allergens = parts[1].split(", ")
            for a in allergens:
                all_allergens.add(a)
            food = {
                "ingredients": ingredients,
                "allergens": allergens
            }
            foods.append(food)

        can_contain = {}
        for food in foods:
            for ing in food["ingredients"]:
                can_contain[ing] = all_allergens.copy()

        for food in foods:
            for a in food["allergens"]:
                # missing ingredients can't contain a present allergen
                for ing in can_contain:
                    if ing not in food["ingredients"]:
                        if a in can_contain[ing]: can_contain[ing].remove(a)

        # how many times an ingredient appears in any food
        ing_count = {}
        for food in foods:
            for ing in food["ingredients"]:
                if ing not in ing_count: ing_count[ing] = 0
                ing_count[ing] += 1

        count = 0
        for ing in can_contain:
            if len(can_contain[ing]) == 0:
                count += ing_count[ing]

        print(count)


s = Solution()
s.solve()
