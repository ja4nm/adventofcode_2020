import re


class Solution:

    def __init__(self):
        pass

    def is_valid(self, vmin, vmax, chr, s):
        c = 0

        for i in range(0, len(s)):
            if s[i] == chr:
                c += 1
                if c > vmax: break

        return c >= vmin and c <= vmax

    def solve(self):
        f = open("res/02a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        valid = 0

        for line in lines:
            m = re.search("(\d+)-(\d+) (\w): (.*)", line)
            vmin = int(m.group(1))
            vmax = int(m.group(2))
            chr = m.group(3)
            password = m.group(4)
            if self.is_valid(vmin, vmax, chr, password):
                valid += 1

        print(valid)


s = Solution()
s.solve()
