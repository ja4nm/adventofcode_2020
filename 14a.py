import re

class Solution:

    def __init__(self):
        self.mask0 = 0
        self.mask1 = 0
        self.mem = dict()
        pass

    def set_mask(self, mask):
        self.mask0 = 0b111111111111111111111111111111111111
        self.mask1 = 0b000000000000000000000000000000000000
        for i in range(0, len(mask)):
            c = mask[i]
            if c == "0":
                self.mask0 ^= (0b1 << len(mask) - i-1)
            elif c == "1":
                self.mask1 ^= (0b1 << len(mask) - i-1)
        #print("    "+mask)
        #print(format(self.mask0, '#040b'))
        #print(format(self.mask1, '#040b'))

    def apply_mask(self, num):
        return (num & self.mask0) | self.mask1

    def solve(self):
        f = open("res/14a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        for line in lines:
            if line.startswith("mask"):
                m = re.match("mask = ([X01]+)", line)
                self.set_mask(m.group(1))
            else:
                m = re.match("mem\[(\d+)\] = (\d+)", line)
                num = int(m.group(2))
                addr = int(m.group(1))
                self.mem[addr] = self.apply_mask(num)

        total = 0
        for num in self.mem.values():
            total += num

        print(total)




s = Solution()
s.solve()
