import copy

class Solution:

    def __init__(self):
        self.shp = [0, 0]
        self.seats = None
        self.seats_tmp = None
        pass

    def in_bounds(self, y, x):
        return x >= 0 and x < self.shp[0] and y >= 0 and y < self.shp[1]

    def can_see_occupied(self, y, x, dy, dx):
        x += dx
        y += dy
        while self.in_bounds(y, x) and self.seats[y][x] != "L":
            if self.seats[y][x] == "#": return True
            x += dx
            y += dy
        return False

    def chg_state(self, y, x):
        if self.seats[y][x] == ".": return False

        # 123
        # 4x5
        # 678
        occupied = 0
        if self.can_see_occupied(y, x, -1, -1): occupied += 1
        if self.can_see_occupied(y, x, -1,  0): occupied += 1
        if self.can_see_occupied(y, x, -1,  1): occupied += 1
        if self.can_see_occupied(y, x,  0, -1): occupied += 1
        if self.can_see_occupied(y, x,  0,  1): occupied += 1
        if self.can_see_occupied(y, x,  1, -1): occupied += 1
        if self.can_see_occupied(y, x,  1,  0): occupied += 1
        if self.can_see_occupied(y, x,  1,  1): occupied += 1

        if self.seats[y][x] == "L" and occupied == 0:
            self.seats_tmp[y][x] = "#"
            return True
        elif self.seats[y][x] == "#" and occupied >= 5:
            self.seats_tmp[y][x] = "L"
            return True

        return False

    def solve(self):
        f = open("res/11a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        seats = []
        for line in lines:
            s = []
            for i in range(0, len(line)):
                s.append(line[i])
            seats.append(s)

        self.seats = seats
        self.seats_tmp = copy.deepcopy(self.seats)
        self.shp = [len(self.seats[0]), len(self.seats)]

        while True:
            changed = 0
            for y in range(0, self.shp[1]):
                for x in range(0, self.shp[0]):
                    is_changed = self.chg_state(y, x)
                    if is_changed: changed += 1
            self.seats = copy.deepcopy(self.seats_tmp)
            if changed == 0: break

        occupied = 0
        for y in range(0, self.shp[1]):
            for x in range(0, self.shp[0]):
                if self.seats[y][x] == "#": occupied += 1

        print(occupied)






s = Solution()
s.solve()
