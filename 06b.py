
class Solution:

    def __init__(self):
        pass

    def solve(self):
        f = open("res/06a/input.txt", "r")
        content = f.read().rstrip()

        count = 0
        groups = content.split("\n\n")

        for group in groups:
            q = dict()
            group_size = 1

            for c in group:
                if c == "\n":
                    group_size += 1
                    continue
                if c not in q: q[c] = 0
                q[c] += 1

            for val in q.values():
                if val >= group_size:
                    count += 1

        print(count)


s = Solution()
s.solve()
