import numpy as np


class Solution:

    def __init__(self):
        pass

    def cnt_valid(self, adapters, i):
        valid = 0
        j = i+1
        while j < len(adapters) and (adapters[i]-adapters[j]) <= 3:
            valid += 1+self.cnt_valid(adapters, j)
            j += 1
        return valid


    def solve(self):
        f = open("res/10a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")

        adapters = [0]
        for line in lines:
            adapters.append(int(line))

        adapters = sorted(adapters)
        adapters.append(adapters[-1] + 3)
        dp = np.zeros(len(adapters))
        dp[-1] = 1

        for i in range(len(adapters)-2, -1, -1):
            k = 0
            for j in range(i+1, len(adapters)):
                if (adapters[j]-adapters[i]) <= 3:
                    k += 1
            dp[i] = max(k, dp[i+1])

            cnt = 0
            for j in range(i+1, len(adapters)):
                if (adapters[j]-adapters[i]) <= 3:
                    cnt += dp[j]
            dp[i] = cnt

        print(dp[0])


s = Solution()
s.solve()
