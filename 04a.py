import re

class Solution:

    def __init__(self):
        self.required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        pass

    def is_passport_valid(self, passport):
        for field in self.required:
            if field not in passport:
                return False
        return True

    def solve(self):
        f = open("res/04a/input.txt", "r")
        content = f.read().rstrip()
        parts = content.split("\n\n")

        valid = 0
        for part in parts:
            found = re.findall(r"([a-z]+):([^\s]+)", part, re.MULTILINE)
            p = {}
            for item in found:
                p[item[0]] = item[1]
            if self.is_passport_valid(p):
                valid += 1

        print(valid)


s = Solution()
s.solve()
