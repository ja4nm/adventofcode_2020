import math

class Solution:

    def __init__(self):
        pass

    def lcm(self, a, b):
        return abs(a * b) // math.gcd(a, b)

    def solve(self):
        f = open("res/13a/input.txt", "r")
        content = f.read().rstrip()
        lines = content.split("\n")
        parts = lines[1].split(",")

        buses = []
        indexes = []
        for i in range(0, len(parts)):
            if parts[i] == "x": continue
            buses.append(int(parts[i]))
            indexes.append(i)

        ts = 0
        lcm = buses[0]

        for i in range(1, len(buses)):
            d = indexes[i]-indexes[i-1]
            j = 0
            while (ts+j*lcm+d) % buses[i] != 0: j += 1
            ts = (ts+j*lcm+d)
            lcm = self.lcm(lcm, buses[i])

        ts = ts-indexes[-1]
        print(ts)


s = Solution()
s.solve()
